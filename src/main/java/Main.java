import java.util.Scanner;

public class Main {
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        Beranda();
    }

    static void Beranda() {
        System.out.println(" ");
        System.out.println("--");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("--");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volum");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("--");
        System.out.println(" ");
        int number = input.nextInt();
        PilihanBeranda(number);
        input.close();
    }

    static void PilihanBeranda(int n) {
        if (n == 1) {
            Bidang();

        } else if (n == 2) {
            Volume();

        } else if (n == 0) {
            System.exit(0);
        } else {
            System.out.println("input tidak diketahui");
            TekanApapun();
        }
    }

    static void Bidang() {
        System.out.println("--");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("--");
        System.out.println("1. persegi");
        System.out.println("2. lingkaran");
        System.out.println("3. segitiga");
        System.out.println("4. persegi panjang");
        System.out.println("0. kembali ke menu sebelumnya");
        System.out.println(" ");
        int pilihBidang = input.nextInt();
        PilihanBidang(pilihBidang);
        input.close();
    }

    static void PilihanBidang(int n) {
        if (n == 1) {
            System.out.println("--");
            System.out.println("Anda memilih persegi");
            System.out.println("--");

            System.out.print("Masukkan sisi: ");
            int sisi = input.nextInt();
            System.out.println("\n processing... \n");
            System.out.println("Luas dari persegi adalah " +
                    LuasPersegi(sisi));
            System.out.println("--");

            TekanApapun();
        } else if (n == 2) {
            System.out.println("--");
            System.out.println("Anda memilih lingkaran");
            System.out.println("--");

            System.out.print("Masukkan jari-jari: ");
            int jari = input.nextInt();
            System.out.println("\n processing... \n");
            System.out.println("Luas dari lingkaran adalah " +
                    LuasLingkaran(jari));
            System.out.println("--");

            TekanApapun();
        } else if (n == 3) {
            System.out.println("--");
            System.out.println("Anda memilih segitiga");
            System.out.println("--");

            System.out.print("Masukkan alas: ");
            int alas = input.nextInt();
            System.out.print("Masukkan tinggi: ");
            int tinggi = input.nextInt();
            System.out.println("\n processing... \n");
            System.out.println("Luas dari segitiga adalah " +
                    LuasSegitiga(alas, tinggi));
            System.out.println("--");

            TekanApapun();
        } else if (n == 4) {
            System.out.println("--");
            System.out.println("Anda memilih persegi panjang");
            System.out.println("--");

            System.out.print("Masukkan panjang: ");
            int panjang = input.nextInt();
            System.out.print("Masukkan lebar: ");
            int lebar = input.nextInt();
            System.out.println("\n processing... \n");
            System.out.println("Luas dari persegi panjang adalah " +
                    LuasPersegiPanjang(panjang, lebar));
            System.out.println("--");


            TekanApapun();
        } else if (n == 0) {
            Beranda();
            int pilihBeranda = input.nextInt();
            PilihanBeranda(pilihBeranda);
            input.close();
        } else {
            System.out.println("Input tidak diketahui");
            TekanApapun();
        }
    }

    static int LuasPersegi(int s) {
        return s * s;
    }

    static double LuasLingkaran(int r) {
        return Math.PI * r * r;
    }

    static double LuasSegitiga(int a, int t) {
        return 0.5 * a * t;
    }

    static int LuasPersegiPanjang(int p, int l) {
        return p * l;
    }

    static void Volume() {
        System.out.println("--");
        System.out.println("Pilih volume yang akan dihitung");
        System.out.println("--");
        System.out.println("1. kubus");
        System.out.println("2. balok");
        System.out.println("3. tabung");
        System.out.println("0. kembali ke menu sebelumnya");
        System.out.println("--");
        int pilihVolume = input.nextInt();
        PilihanVolume(pilihVolume);
        input.close();
    }

    static void PilihanVolume(int n) {
        if (n == 1) {
            System.out.println("--");
            System.out.println("Anda memilih Kubus");
            System.out.println("--");

            System.out.print("Masukkan sisi: ");
            int sisi = input.nextInt();
            System.out.println("\n processing... \n");
            System.out.println("Volume dari kubus adalah " +
                    VolumeKubus(sisi));
            System.out.println("--");

            TekanApapun();

        } else if (n == 2) {
            System.out.println("--");
            System.out.println("Anda memilih Balok");
            System.out.println("--");

            System.out.print("Masukkan panjang: ");
            int panjang = input.nextInt();
            System.out.print("Masukkan lebar: ");
            int lebar = input.nextInt();
            System.out.print("Masukkan tinggi: ");
            int tinggi = input.nextInt();
            System.out.println("\n processing... \n");
            System.out.println("Volume dari Balok adalah " +
                    VolumeBalok(panjang, lebar, tinggi));
            System.out.println("--");

            TekanApapun();
        } else if (n == 3) {
            System.out.println("--");
            System.out.println("Anda memilih Tabung");
            System.out.println("--");

            System.out.print("Masukkan jari-jari: ");
            int jari = input.nextInt();
            System.out.println("Masukkan tinggi: ");
            int tinggi = input.nextInt();
            System.out.println("\n processing... \n");
            System.out.println("Volume dari tabung adalah " +
                    VolumeTabung(jari, tinggi));
            System.out.println("--");

            TekanApapun();
        } else if (n == 0) {
            Beranda();
            int pilihBeranda = input.nextInt();
            PilihanBeranda(pilihBeranda);
            input.close();
        } else {
            System.out.println("Input tidak diketahui");
            TekanApapun();
        }
    }

    static int VolumeKubus(int s) {
        return s * s * s;
    }

    static int VolumeBalok(int p, int l, int t) {
        return p * l * t;
    }

    static double VolumeTabung(int r, int t) {
        return Math.PI * r * r * t;
    }

    static void TekanApapun() {
        System.out.print("tekan apapaun untuk kembali ke menu utama dan enter...");
        input.nextLine();
        input.nextLine();
        Beranda();
    }
}
